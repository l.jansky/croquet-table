import 'package:flutter/material.dart';

enum PlayerColor {
  green,
  red,
  yellow,
  blue,
  orange,
  black
}

const playerColorMap = {
  PlayerColor.green: Colors.green,
  PlayerColor.red: Colors.red,
  PlayerColor.yellow: Colors.yellow,
  PlayerColor.blue: Colors.blue,
  PlayerColor.orange: Colors.orange,
  PlayerColor.black: Colors.black
};

Color getPlayerColor(PlayerColor playerColor) => playerColorMap[playerColor];

class PlayerHit {
  final PlayerColor color;
  final bool hit;

  PlayerHit(this.color, this.hit);
}

class Player {
  final PlayerColor color;
  final Set<PlayerColor> hits;
  int nextPathIndex;
  final List<int> penaltyGates;

  Player({ this.color, this.nextPathIndex = 0, this.hits = const {}, this.penaltyGates = const [] });

  Player hitBall(PlayerColor ball) {
    return Player(
      color: color,
      nextPathIndex: nextPathIndex,
      hits: {...hits, ball}
    );
  }

  Player removeHit(PlayerColor ball) {
    return Player(
      color: color,
      nextPathIndex: nextPathIndex,
      hits: hits.where((hit) => hit != ball).toSet()
    );
  }

  Player pass() {
    return Player(
      color: color,
      nextPathIndex: nextPathIndex+1,
      hits: hits
    );
  }

  List<PlayerHit> getPlayerHits(List<PlayerColor> players) {
    return players.map((player) => PlayerHit(player, hits.contains(player))).toList();
  }
}