import 'package:croquet_table/models/game.dart';
import 'package:flutter/foundation.dart';

class GameList extends ChangeNotifier {
  List<Game> _games = [];

  List<Game> get games => _games;

  void add(Game game) {
    _games.add(game);
    notifyListeners();
  }

  void update(Game game) {
    _games = _games.map((g) => g.name == game.name ? game : g).toList();
    notifyListeners();
  }

  Game getGameByName(String name) {
    return _games.firstWhere((element) => element.name == name);
  }
}