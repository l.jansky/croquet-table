import 'package:croquet_table/models/player.dart';

class Game {
  final String name;
  final List<Player> players;
  final List<int> gamePath;  // 0 for stake, >0 for wicket

  Game({ this.name, this.players, this.gamePath });

  Game.initialize(String name, int numberOfPlayers, int numberOfWickets) : 
    name = name,
    players = Game.generatePlayers(numberOfPlayers),
    gamePath = Game.generateGamePath(numberOfWickets);

  Game hitBall(PlayerColor byPlayer, PlayerColor ball) {
    return Game(
      name: name,
      players: players
        .map(
          (player) => player.color == byPlayer ? player.hitBall(ball) : player
        )
        .toList(),
      gamePath: gamePath
      );
  }

  Game pass(PlayerColor byPlayer) {
    final player = players.firstWhere((player) => player.color == byPlayer);
    final wicket = gamePath[player.nextPathIndex] > 0;

    return Game(
      name: name,
      players: players
        .map((player) {
          if (player.color == byPlayer) {
            return player.pass();
          }

          return wicket ? player.removeHit(byPlayer) : player;
        })
        .toList(),
      gamePath: gamePath
    );
  }

  static List<int> generateGamePath(int numberOfWickets) {
    final List<int> gamePath = [];
    for (int i = 0; i < numberOfWickets; i++) {
      if (i == (numberOfWickets/2).floor()) {
        gamePath.add(0);
      }
      gamePath.add(i+1);
    }
    gamePath.add(0);
    return gamePath;
  }

  static List<Player> generatePlayers(int numberOfPlayers) {
    const orderedColors = [PlayerColor.green, PlayerColor.red, PlayerColor.yellow, PlayerColor.blue, PlayerColor.orange, PlayerColor.black];
    return orderedColors
      .sublist(0, numberOfPlayers)
      .map((color) => Player(color: color))
      .toList();
  }
}