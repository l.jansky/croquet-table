import 'package:croquet_table/common/theme.dart';
import 'package:croquet_table/models/game_list.dart';
import 'package:croquet_table/screens/create_game/create_game.dart';
import 'package:croquet_table/screens/game_list/game_list.dart';
import 'package:croquet_table/screens/game_progress/game_progress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GameList())
      ],
      child: MaterialApp(
        title: 'Croquet table',
        theme: appTheme,
        initialRoute: '/',
        routes: {
          '/': (context) => GameListScreen(),
          '/create': (context) => CreateGameScreen(),
          '/progress': (context) => GameProgressScreen()
        },
      ));
  }
}

