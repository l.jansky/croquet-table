// import 'package:croquet_table/models/game.dart';
import 'package:croquet_table/models/game.dart';
import 'package:croquet_table/models/game_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class CreateGameScreen extends StatefulWidget {
  @override
  _CreateGameScreenState createState() => _CreateGameScreenState();
}

class _CreateGameScreenState extends State<CreateGameScreen> {
  final emailFieldController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String name;
  int numberOfPlayers;
  int numberOfWickets = 10;

  void _submit(context) {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      Provider
        .of<GameList>(context, listen: false)
        .add(Game.initialize(name, numberOfPlayers, numberOfWickets));
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create game', style: Theme.of(context).textTheme.headline1),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        padding: EdgeInsets.all(24),
        children: [
          Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextFormField(
                  onSaved: (val) {
                    setState(() {
                      name = val;
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Enter name',
                    labelText: 'Name'
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                TextFormField(
                  onSaved: (val) {
                    setState(() {
                      numberOfPlayers = int.parse(val);
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Enter number of players',
                    labelText: 'Number of players'
                  ),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
                SizedBox(
                  height: 24,
                ),
                new DropdownButtonFormField<String>(
                  items: <String>['8', '10'].map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  value: numberOfWickets.toString(),
                  onChanged: (val) {
                    setState(() {
                      numberOfWickets = int.parse(val);
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Enter number of gates',
                    labelText: 'Number of gates'
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                ElevatedButton(
                  onPressed: () {
                    _submit(context);
                  },
                  child: Text('Save'),
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}
