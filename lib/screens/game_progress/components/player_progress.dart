import 'package:croquet_table/models/player.dart';
import 'package:flutter/material.dart';

class PlayerProgress extends StatelessWidget {
  final Player player;
  final List<PlayerColor> otherPlayers;
  final void Function(PlayerColor hit) hit;
  final void Function() pass;

  PlayerProgress({ @required this.player, @required this.otherPlayers, this.hit, this.pass });

  @override
  Widget build(BuildContext context) {
    final hits = player.getPlayerHits(otherPlayers);
    print('rebuild');
    return GridView.count(
      crossAxisCount: 3,
      children: [
        ...hits
          .map(
            (hit) => GestureDetector(
              child: Icon(
                hit.hit ? Icons.not_interested : Icons.face,
                color: getPlayerColor(hit.color),
                size: 100,
              ),
              onTap: () {
                this.hit(hit.color);
              },
            )
          )
          .toList(),
          GestureDetector(
            child: Icon(
              Icons.input,
              color: Colors.black,
              size: 100,
            ),
            onTap: () {
              pass();
            },
          )
      ]
    );
  }
}