import 'package:croquet_table/models/game_list.dart';
import 'package:croquet_table/models/player.dart';
import 'package:croquet_table/screens/game_progress/components/player_progress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GameProgressArguments {
  final String gameName;

  GameProgressArguments(this.gameName);
}

class GameProgressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final GameProgressArguments args = ModalRoute.of(context).settings.arguments;

    return Consumer<GameList>(builder: (context, gameList, child) {
      final game = gameList.getGameByName(args.gameName);
      return DefaultTabController(
        length: game.players.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Game progress', style: Theme.of(context).textTheme.headline1),
            backgroundColor: Colors.white,
          ),
          body: TabBarView(
            children: game.players.map((player) => PlayerProgress(
              player: player,
              otherPlayers: game.players
                .where((p) => p.color != player.color)
                .map((player) => player.color).toList(),
              hit: (ball) {
                gameList.update(game.hitBall(player.color, ball));
              },
              pass: () {
                gameList.update(game.pass(player.color));
              }
            )).toList()
          ),
          bottomNavigationBar: TabBar(
            tabs: game.players.map((player) => Tab(
              icon: Icon(Icons.face, color: getPlayerColor(player.color))
            )).toList(),
            indicatorColor: Colors.brown,
          ),
        )
      );
    });
  }
}
