import 'package:croquet_table/models/game_list.dart';
import 'package:croquet_table/screens/game_progress/game_progress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GameListScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Games', style: Theme.of(context).textTheme.headline1),
        backgroundColor: Colors.white,
      ),
      body: Consumer<GameList>(builder: (context, gameList, child) {
        return ListView(
          children: gameList.games.map((game) {
            return GestureDetector(
              child: Container(
                height: 50,
                color: Colors.amber[600],
                child: Center(child: Text(game.name)),
              ),
              onTap: () {
                Navigator.of(context).pushNamed('/progress', arguments: GameProgressArguments(game.name));
              },
            );
          }).toList(),
        );
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/create');
        },
        tooltip: 'Create game',
        child: Icon(Icons.add),
      )
,
    );
  }
}