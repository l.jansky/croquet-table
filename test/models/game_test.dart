import 'package:croquet_table/models/player.dart';
import 'package:test/test.dart';
import 'package:croquet_table/models/game.dart';

void main() {
  group('Game model', () {
    test('ball should be set as hit after player hits it', () {
      final game = Game(name: 'test', players: [
        Player(color: PlayerColor.green),
        Player(color: PlayerColor.blue),
        Player(color: PlayerColor.red)
      ]);

      final gameAfterHit = game
        .hitBall(PlayerColor.green, PlayerColor.blue)
        .hitBall(PlayerColor.green, PlayerColor.red)
        .hitBall(PlayerColor.red, PlayerColor.green);

      expect(gameAfterHit.players[0].hits, equals([PlayerColor.blue, PlayerColor.red]));
      expect(gameAfterHit.players[1].hits, equals([]));
      expect(gameAfterHit.players[2].hits, equals([PlayerColor.green]));
    });

    test('players ball should be removed from others players hits after he passes correct gate', () {
      final game = Game(
        name: 'test',
        gamePath: [1, 0],
        players: [
          Player(color: PlayerColor.green, hits: { PlayerColor.blue, PlayerColor.red }),
          Player(color: PlayerColor.blue, hits: { PlayerColor.red }),
          Player(color: PlayerColor.red, hits: { PlayerColor.blue })
        ]);

      final gameAfterPass = game.pass(PlayerColor.red);
      expect(gameAfterPass.players[0].hits, equals([PlayerColor.blue]));
      expect(gameAfterPass.players[1].hits, equals([]));
      expect(gameAfterPass.players[2].hits, equals([PlayerColor.blue]));
      expect(gameAfterPass.players[2].nextPathIndex, 1);
    });

    test('players ball should not be removed if he passes a stake', () {
      final game = Game(
        name: 'test',
        gamePath: [1, 0, 2],
        players: [
          Player(color: PlayerColor.green, hits: { PlayerColor.blue }),
          Player(color: PlayerColor.blue, nextPathIndex: 1)
        ]);

      final gameAfterPass = game.pass(PlayerColor.blue);
      expect(gameAfterPass.players[0].hits, equals([PlayerColor.blue]));
      expect(gameAfterPass.players[1].hits, equals([]));
      expect(gameAfterPass.players[1].nextPathIndex, 2);
    });

    test('initialize game', () {
      final game = Game.initialize('test', 4, 8);
      expect(game.gamePath, equals([1, 2, 3, 4, 0, 5, 6, 7, 8, 0]));
    });

    test('generate game path', () {
      final gamePath = Game.generateGamePath(10);
      expect(gamePath, equals([1, 2, 3, 4, 5, 0, 6, 7, 8, 9, 10, 0]));
    });

    test('generate players', () {
      final players = Game.generatePlayers(3);
      final colors = players.map((player) => player.color);
      expect(colors, equals([PlayerColor.green, PlayerColor.red, PlayerColor.yellow]));
    });
  });
}